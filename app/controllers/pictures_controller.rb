class PicturesController < ApplicationController
  before_action :find_picture, only: [:show, :edit, :update, :destroy]

  def index
    if params[:tag]
      @pictures = Picture.tagged_with(params[:tag])
    else
      @pictures = current_user.pictures
    end
    @categories = Category.all
    @tags = Picture.all.tag_counts
    # @pictures = Picture.all
    # @categories = Category.all
    # @tags = Picture.all.tag_counts
  end

  def new
    @picture = current_user.pictures.build
  end

  def create
    @picture = current_user.pictures.build(picture_params)

    if @picture.save
      redirect_to user_pictures_path, notice: "Sucsessfully created new Post"
    else
      render 'new'
    end
  end

  def edit

  end

  def update
    if @picture.update(picture_params)
      redirect_to user_pictures_path, notice: "Picture was Succsessfully updates!"
    else
      render 'edit'
    end
  end

  def destroy
    @picture.destroy
    redirect_to user_pictures_path
  end

  private

  def find_picture
    @picture = Picture.find(params[:id])
  end

  def picture_params
    params.require(:picture).permit(:title, :image, :category_id, :tag_list)
  end
end
