class SearchesController < ApplicationController
  def new
    @search = Search.new
    @tags = Picture.all.tag_counts

  end

  def create
    @search = Search.create!(search_params)
    redirect_to @search
  end

  def show
    @search = Search.find(params[:id])
    @pictures = @search.pictures
  end

  private

  def search_params
    params.require(:search).permit(:category_id, :tag_id)
  end
end
