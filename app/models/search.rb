class Search < ApplicationRecord
  def pictures
    @pictures ||= find_pictures
  end

  def find_pictures
    results = Picture.includes(:taggings)
    results = results.where(category_id: category_id)
    results = results.where( taggings: { tag_id: tag_id })
    # byebug
    # results = results.where("tag_id = ?", params[:tag_id]) if tag_id.present?
    # byebug
    results
  end
end

