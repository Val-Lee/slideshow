class Picture < ApplicationRecord
  validates :title, :image, presence: true
  belongs_to :user
  belongs_to :category
  mount_uploader :image, ImageUploader
  acts_as_taggable
end
