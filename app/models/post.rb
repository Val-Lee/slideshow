class Post < ApplicationRecord
  belongs_to :user
  belongs_to :category
  scope :published, -> { where(published: true) }
end
