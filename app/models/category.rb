class Category < ApplicationRecord
  has_many :posts
  has_many :pictures
  has_ancestry
  validates :name, presence: true
end
