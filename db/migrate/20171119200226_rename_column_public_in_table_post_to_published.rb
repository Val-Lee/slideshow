class RenameColumnPublicInTablePostToPublished < ActiveRecord::Migration[5.1]
  def change
  	rename_column :posts, :public, :published
  end
end
