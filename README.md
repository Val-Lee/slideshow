# Slideshow

## Installation

* Install gems
```sh
$ gem install bundler
$ bundle
```

* Configure database
```sh
$ bin/rails db:migrate
```
