Rails.application.routes.draw do
  devise_for :users, controllers: {omniauth_callbacks: "omniauth_callbacks"}
  resources :users do
    resources :pictures
  end
  resources :posts
  resources :categories
  resources :searches
  get 'tags/:tag', to: 'pictures#index', as: :tag
  root "posts#index"
end
